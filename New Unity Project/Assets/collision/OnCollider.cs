﻿using UnityEngine;
using System.Collections;

public class OnCollider : MonoBehaviour {

	public float haveforce=12f;
	
	void OnCollisionEnter( Collision collider){
		Debug.Log("enter collision");
	}

	void OnCollisionStay (Collision collider){
		Debug.Log("stay collision");
	}

	void OnCollisionExit (Collision collider){
		Debug.Log("Exit collision");
	}




	void OnTriggerEnter( Collider collider){
		Debug.Log("OnTriggerEnter collision");
	}
	
	void OnTriggerStay (Collider collider){
		collider.rigidbody.AddForce (Vector3.up * haveforce, ForceMode.Acceleration);
		Debug.Log("OnTriggerStay collision");
	}
	
	void OnTriggerExit (Collider collider){
		Debug.Log("OnTriggerExit collision");
	}



}
